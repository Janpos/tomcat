<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Alle Artikel</title>
   <link href="Design.css" rel="stylesheet">
</head>
<body>

<div class="sidenav">
  <a href="/about">�ber uns</a>
  <a href="/Waren">Waren</a>
  <a href="/Retouren">Retouren</a>
  <a href="/Kontakt">Kontakt</a>
  <a href="/FAQ">FAQ</a>
  <button class="dropdown-btn">Dropdown 
    <i class="fa fa-caret-down"></i>
  </button>
  <div class="dropdown-container">
    <a href="#">Komplett-Pc's</a>
    <a href="#">Einzelartikel</a>
    <a href="#">Sonstige-Artikel</a>
    <a href="#">Zusatz-Artikel</a>
    <a href="#">Zubeh�r</a>
  </div>
  <a href="/search">Search</a>
</div>

<script src="DropDown.js"></script>


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<ul>
		<c:forEach items="${artikelListe }" var="item">
	        <li>
	        	<a href="Artikelansicht?Artikel=${item.artikelNr }">${item.name }</a><br/>
	        </li>
	    </c:forEach>
	</ul>
</body>
</html>