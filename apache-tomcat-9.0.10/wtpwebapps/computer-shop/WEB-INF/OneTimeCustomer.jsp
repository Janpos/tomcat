<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
 <meta charset="ISO-8859-1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Kundeninformationen</title>
    <link href="Design.css" rel="stylesheet">
</head>
<body>

<div class="sidenav">
  <a href="/about">�ber uns</a>
  <a href="/Waren">Waren</a>
  <a href="/Retouren">Retouren</a>
  <a href="/Kontakt">Kontakt</a>
  <a href="/FAQ">FAQ</a>
  <button class="dropdown-btn">Dropdown 
    <i class="fa fa-caret-down"></i>
  </button>
  <div class="dropdown-container">
    <a href="#">Komplett-Pc's</a>
    <a href="#">Einzelartikel</a>
    <a href="#">Sonstige-Artikel</a>
    <a href="#">Zusatz-Artikel</a>
    <a href="#">Zubeh�r</a>
  </div>
  <a href="/search">Search</a>
</div>

<script src="DropDown.js"></script>

<p align="right">

<a href="https://www.google.com/" title="https://www.google.com/"><input style="z-index: 0; filter: ; width: 7em; height: 2em; id="text" value="Einloggen" type="button" name="LogOut" /></a>

<a href="https://www.google.com/" title="https://www.google.com/"><input style="z-index: 0; filter: ; width: 7em; height: 2em; id="text" value="Ausloggen" type="button" name="LogIn" /></a>

<a href="/computer-shop/Warenkorb.jsp"><img src="https://www.phono-schop.ch/templates/csr-records-bootstrap/img/warenkorb_white.png" width="20em" height="20em" alt="Warenkorb" /></a>

</p>

	<h1>Kundeninformationen</h1>
	<form action="OneTimeCustomer" method="post">
		
		<h2>
			Persoenliche Daten
		</h2>
		
		<p>
			Titel:
			<input type="text" name="title">
		</p>
		
		<p>
			Anrede
    		<select name="salutation">
        	<option value="Herr">Herr</option>
        	<option value="Frau">Frau</option>
        	<option value="Anderes">Anderes</option>

    		</select>
    	</p>
    	
    	<p>
    		Nachname
			Name: <input type="text" name="lastName">
    	</p>
    	 <p>
    		Vorname
			Name: <input type="text" name="firstName">
    	</p>
    	
    	<h2>
			Lieferadresse
    	</h2>
    	
    	<p>
    		Strasse: <input type="text" name="street">
    		Hausnummer: <input type="text" name="houseNumer">
    	</p>
    	    
    	<p>
    		Ort: <input type="text" name="place">
    		PLZ: <input type="text" name="postcode">
    	</p>
    	
    	<h2>
    		Lieferadresse
    	</h2>
    	<p>
    		Geben Sie eine Rechnungsadresse ein, falls diese von der Lieferadresse abweicht
    	</p>
    	
    	<p>
    		Strasse: <input type="text" name="optStreet">
    		Hausnummer: <input type="text" name="optHouseNumer">
    	</p>
    	
    	<p>
    		Ort: <input type="text" name="optPlace">
    		PLZ: <input type="text" name="optPostcode">
    	</p>
    	
    	<h2>
    		Versandart
    	</h2>
    	<p>
    		Versandart:
    		<input type="radio" name="delivery" value="Standard" /> Standard (2-3 Werktage)
    	
    		<input type="radio" name="delivery" value="Express" /> Express (Lieferung am naechsten Tag, 5 Euro)
    	</p>
    	

		<p>
			<input type="submit" name="submit" value="Kostenpflichtig bestellen">
	</form>

</body>
</html>