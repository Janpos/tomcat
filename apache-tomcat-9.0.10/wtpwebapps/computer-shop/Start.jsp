<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Startseite</title>
</head>
<body style="background-color:powderblue;">
	<h1> Hallo </h1>
	<%
         out.println("Your IP address is " + request.getRemoteAddr() + "<br>");
	
    	for(int i=1;i<=10;i++){
    		out.println("Die Zahl ist " + i + "<br>");
    	}
    %>
    
     <p>Benutzername        
    <input type="text" name="name" /></p>

    <p>Passwort       
    <input type="password" name="pass" /></p>

    <p>Geschlecht   
    <input type="radio" name="gender" value="M" /> Mann
    <input type="radio" name="gender" value="F" /> Frau
    <input type="radio" name="gender" value="O"/> Anderes</p>

    <p>AGB
    <input type="checkbox" name="agree" /> Zustimmen</p>

    <p>Multi-selection checkboxes.
    <input type="checkbox" name="role" value="USER" /> User
    <input type="checkbox" name="role" value="ADMIN" /> Admin</p>

    <p>Land
    <select name="countryCode">
        <option value="NL">Deutschland</option>
        <option value="US">England</option>
        <option value="US">Indien</option>
        <option value="US">Ist mir egal</option>
    </select></p>

    <p>Multi-selection listbox.
    <select name="animalId" multiple="true" size="2">
        <option value="1">a</option>
        <option value="2">b</option>
        <option value="2">c</option>
        <option value="2">d</option>
        <option value="2">e</option>
        <option value="2">f</option>
        <option value="2">g</option>
    </select></p>

    <p>Text area.
    <textarea name="message"></textarea></p>

    <p>Submit button.
    <input type="submit" name="submit" value="submit" /></p>
</body>
</html>